

#[derive(Debug,Serialize, Deserialize)]
struct Claim {
    crt: usize, // when this was created
    exp: usize, // expiration date for the claim
}

async fn bro() {
    let id = 123;
    let now = chrono::Utc::now()
        .timestamp();

    let  future = chrono::Utc::now()
        .checked_add_signed(chrono::Duration::minutes(1))
        .expect("Invalid timestampe thing")
        .timestamp();

    let claim = Claim {
        crt: now as usize,
        exp: future as usize
    };

    let secret = b"rando msecret owo";
    let mut header = Header::new(Algorithm::HS512);
    header.cty = Some("JSON".into());
    header.kid = Some(format!("{}-{}", id, now)); // uid can go here

    let enc = encode(&header, &claim, &EncodingKey::from_secret(secret));
    println!("Encoded: {:?}", enc);
    let dk = DecodingKey::from_secret(secret);
    println!("Decoded: {:?}", decode::<Claim>(&enc.unwrap(), &dk, &Validation::new(Algorithm::HS512)));

    let random = "some.random.key";
    println!("User provided key: {} => {:?}", random , decode::<Claim>(random, &dk, &Validation::new(Algorithm::HS512)));


}
