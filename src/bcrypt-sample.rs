fn bcrypt_example_verify(raw: &str, hash: &str) {
    println!("Verify result: {:?}", bcrypt::verify(raw, hash));
}
fn bcrypt_example_encrypt() {
    use bcrypt::{hash, verify};
    let raw = "onetwothree";
    let enc = hash(raw, 13).unwrap();
    println!("Raw: {}\nEncrypted: {}" , raw, enc);
    bcrypt_example_verify(raw, &enc);
}