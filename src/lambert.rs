#[derive(Copy, Clone)]
pub struct Pvec {
    pub x: f64,
    pub y: f64
}

impl Pvec {
    pub fn new(x: f64, y: f64) -> Self {
        return Self {
            x: x,
            y: y
        };
    }

    pub fn dist(p1: Pvec, p2: Pvec) -> f64 {
        let left = (p2.x - p1.x).powi(2);
        let rght = (p2.y - p1.y).powi(2);

        return (left - rght).sqrt();
    }

    pub fn magnitude(&self) -> f64 {
        return Pvec::dist(*self, Pvec {x: 0.0, y: 0.0});
    }
}

pub struct Hyperbola {
    pub h: f64,
    pub k: f64,
    pub f1: Pvec,
    pub f2: Pvec,

    pub major_axis: f64,
    pub minor_axis: f64,

    pub asym_up: f64,
    pub asym_down: f64,

    pub ecc: f64
}

impl Hyperbola {
    pub fn new(f1: Pvec, f2: Pvec) -> Self {
        let h = (f1.x + f2.x) / 2.0;
        let k = (f1.y + f2.y) / 2.0;

        let center = Pvec::new(h, k);
        let focal_distance = Pvec::dist(center, f2);
        let a = focal_distance * 2.0 / 3.0;

        return Self {
            h: h,
            k: k,

            f1: f1,
            f2: f2,

            major_axis: a,
            minor_axis: a,

            asym_up: 1.0,
            asym_down: -1.0,

            // sqrt( (a^2) + (b^2) ) / a
            ecc: (a.powi(2) + a.powi(2)).sqrt() / a
        }
    }

    pub fn point_relative(&self, x:f64, y:f64) -> f64 {
        let left = (x-self.h).powi(2) / self.major_axis.powi(2);
        let rght = (y-self.k).powi(2) / self.minor_axis.powi(2);

        return left - rght - 1.0
    }

    pub fn close(&mut self) {
        self.ecc = self.ecc + 1.0 / 2.0;
    }

    pub fn open(&mut self) {
        self.ecc = self.ecc * 1.5;
    }
}