use std::convert::Infallible;
use std::collections::HashMap;
use std::net::SocketAddr;

use tokio;

use hyper::{
    self, Body, Request,
    Method, Server,
    Response, StatusCode,
    service:: {
        make_service_fn, service_fn
    }
};

async fn permissiable_request_confirmation(query_map: HashMap<String, String>) -> Result<(), &'static str>{
    match query_map.get("key") {
        Some(value) => {
            // Pull key data from key cache (key, limit, delay)
            // If that key has a `limit` on it then ensure we haven't gone over that limit
            // optionally that key data can contain a field to reset the limit but that would be
            // more cleanly dealt with by another service to seperate out the logic
            Ok(())
        },
        None => {
            Err("User not allowed")
        }
    }
}

async fn auth_wall(request: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    // check if a key is provided
    unimplemented!()
}

async fn response_dispatcher(request: Request<Body>) -> Result<Response<Body>, hyper::Error>{
    let mut response = Response::new(Body::empty());

    println!("{}=>{}", request.method(), request.uri().path());
    match(request.method(), request.uri().path()) {

        /* control route */
        (&Method::GET, "/") => *response.status_mut() = StatusCode::OK,
        /* route makes sure we don't go over our use limit */
        (&Method::GET, "/cummulative") => {
        },
        /* route makes sure we don't spam too fast */
        (&Method::GET, "/velocity") => {
        },
        _ => *response.status_mut() = StatusCode::NOT_FOUND
    }
    Ok(response)
}

async fn shutdown_signal() {
    tokio::signal::ctrl_c()
        .await
        .expect("failed to shutdown normally");
}

#[tokio::main]
async fn main() {
    println!("Serving on localhost:8080");
    let addr = SocketAddr::from(([127,0,0,1], 8080));

    let service = make_service_fn(|_conn| async {
        Ok::<_, Infallible>(service_fn(response_dispatcher))
    });

    let server = Server::bind(&addr).serve(service);
    let graceful_shutdown = server.with_graceful_shutdown(shutdown_signal());

    if let Err(e) = graceful_shutdown.await {
        eprintln!("Server error: {}", e);
    }
}
