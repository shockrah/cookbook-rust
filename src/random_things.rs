extern crate getrandom;
extern crate rand;
use getrandom::getrandom;
use std::char;

fn random() -> u64 {
    let mut buf = [0u8;8];
    match getrandom(&mut buf) {
        Ok(_v) => {u64::from_ne_bytes(buf)}
        Err(_e) => {0}
    }
}

fn random_string() -> String {
    // Valid ascii char set from 33 - 126
    let mut slice = [0u8; 32];
    match getrandom(&mut slice) {
        Ok(_ignored) => {
            let mut buf = String::new();
            for i in slice.iter() {
                let mut x: u8 = *i;
                if x > 126 { x %= 126; }
                if x < 33 { x += 34; }
                buf.push(x as char);
            }
            buf
        }
        Err(_e) => {
            "Shit's busted".into()
        }
    }
}
