fn _args_test() {
    let args = App::new("testing")
        .version("1.0")
        .arg(Arg::with_name("lad")
             .short("l")
             .value_name("_lad")
             .takes_value(true))
        .get_matches();

    if let Some(lad) = args.value_of("lad") {
        println!("{}", lad);
    }
}

