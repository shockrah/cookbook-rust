// Example of a smallish text based api for minichat
use {
    hyper::{
        service::{make_service_fn, service_fn},
        Body,
        Client,
        Response,
        Request,
        Server,
        Uri,
    },
    std::net::SocketAddr,
};

async fn serve_req(_req: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    Ok(Response::new(Body::from("swag")))
}

async fn start_server(addr: SocketAddr) {
    println!("Listeneing on http://{}", &addr);

    let server = Server::bind(&addr)
        .serve(make_service_fn(|_| {
            async {
                {
                    Ok::<_, hyper::Error>(service_fn(serve_req))
                }
            }
        }));

    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}

#[tokio::main]
async fn main() {
    let addr = SocketAddr::from(([127,0,0,1], 3000));

    start_server(addr).await;
}
