mod lambert;
use lambert::{Hyperbola, Pvec};

fn main() {
    let r1 = Pvec::new(1.0, 5.0);
    let r2 = Pvec::new(5.0, 5.0);

    let mut hyper = Hyperbola::new(r1, r2);
    println!("Before adjustment: {}", hyper.ecc);
    hyper.close();
    println!("After adjustment: {}", hyper.ecc);
}