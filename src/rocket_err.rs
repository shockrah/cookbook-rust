#![feature(proc_macro_hygiene, decl_macro, plugin)]
#[macro_use] extern crate rocket;

use std::error;
use std::fmt;
use rocket::response::{self, Responder, Response};
use rocket::http::{Status, ContentType};
use rocket::request::Request;


type ServerResult<T, ServerErr> = std::result::Result<T, ServerErr>;

#[derive(Debug, Clone)]
struct ServerErr;

// Formatter for the error(required)
impl fmt::Display for ServerErr {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "fmt'd display for ServerError err")
    }
}
// Required because we need to implement errors for the error type
impl error::Error for ServerErr {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

impl<'r> Responder<'r> for ServerErr {
    fn respond_to(self, _:&Request) -> response::Result<'r> {
        Response::build()
            .status(Status::InternalServerError)
            .raw_header("server-problem", "db busted")
            .header(ContentType::new("application", "database"))
            .ok()
    }
}

#[get("/opt/<param>")]
fn get_opt(param: i32) -> Option<String> {
    if param == 1 {
        Some("asdf".into())
    }
    else{
        None
    }
}

#[get("/res/<res>")]
fn get_err(res: i32) -> Result<String, ServerErr>  {
    if res == 1 {
        Ok("asdf".into())
    }
    else {
        println!("There was a problem");
        Err(ServerErr{})
    }
}
