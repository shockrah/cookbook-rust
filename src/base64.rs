fn base64_example() {

    let mut buf: Vec<u8> = vec![0;64];
    println!("Making buffer random");
    getrandom(&mut buf).unwrap();
    let string = base64::encode(&buf);

    // encode then dump
    for i in buf.iter() {
        print!("{} ", i);
    }
    let sample = Example{id: 123, name: "name".into()};
    println!("\nLength: {}\nEncoded: {}\n\tEncoded Length: {}",buf.len(), string, string.len());
    println!("{:?}", sample);
    println!("From serde_json::to_string(&example): {}", serde_json::to_string(&sample).unwrap());
}