// Example showing how the classic threading example in C/C++
// can be written in Rust and the benefits of doing so
use std::thread;
use std::sync::mpsc;
use std::time::Duration;
use rand::{self, Rng};

fn worker(load: u64) {
    println!("start, {}", load);
    thread::sleep(Duration::from_millis(load));
    println!("end, {}", load);
}
pub fn load_balancer() {
    // Here we generate workloads
    let mut rng = rand::thread_rng();
    // With this channel we can talk to our worker threads
    //let (tx, rx) = mpsc::channel();
    // spawn our threads here
    for i in 1 .. 100 {
        let workload: u64 = rng.gen_range(1, 100);
        println!("load, {}, {}", i, workload);
    }
}